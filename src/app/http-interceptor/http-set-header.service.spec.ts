import { TestBed } from '@angular/core/testing';

import { HttpSetHeaderService } from './http-set-header.service';

describe('HttpSetHeaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpSetHeaderService = TestBed.get(HttpSetHeaderService);
    expect(service).toBeTruthy();
  });
});
