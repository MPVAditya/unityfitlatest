import { Injectable } from '@angular/core';
import { HttpEvent } from '@angular/common/http';
import { HttpHandler, HttpRequest, HttpInterceptor } from '@angular/common/http';

@Injectable()
export class DataFormatInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
  
      console.log(req.body);
      const formateData = this.convertIntoEncodedString(req.body);
      const formatedRequest = req.clone( {body: formateData} );
      return next.handle(formatedRequest);
  }

  convertIntoEncodedString(obj: any) {
    const str = [];
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
      }
    }
    return str.join('&');
  }

  constructor() { }
}
