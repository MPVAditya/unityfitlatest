import { TestBed } from '@angular/core/testing';

import { DataFormatInterceptorService } from './data-format-interceptor.service';

describe('DataFormatInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataFormatInterceptorService = TestBed.get(DataFormatInterceptorService);
    expect(service).toBeTruthy();
  });
});
