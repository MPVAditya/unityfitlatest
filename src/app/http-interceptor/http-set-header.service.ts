import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpSetHeaderService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const contentHeader = {'Content-Type': 'application/x-www-form-urlencoded'};
    const reqHeaders = req.clone({setHeaders: contentHeader});
    console.log(reqHeaders);
    return next.handle(reqHeaders);
  }

  constructor() { }
}
