import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataFormatInterceptorService } from './data-format-interceptor.service';
import { HttpSetHeaderService } from './http-set-header.service';

export const httpInterceptorProviders = [
    {provide: HTTP_INTERCEPTORS, useClass: HttpSetHeaderService, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: DataFormatInterceptorService,  multi: true },
];


