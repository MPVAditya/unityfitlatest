import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  db = null;
  constructor(private router: Router) {
    this.db = new PouchDB('fitness.db');
   }
  
  AddLoginDetailsToPouchDB(loginDetails, userDetails: LoginDetailsToSend, url:string)
  {
    let userObject = {};
    this.db.get("currentUser")
    .then(doc => {
        doc.username = userDetails.username;
        doc.password = userDetails.password;
        doc.userid = loginDetails.user_id;
        doc.platform = url;
        this.db.put(doc);
        this.router.navigate(['tabs/tabs/tab1']);
    }).catch(err => {
        if (err.status === 404) {
            userObject['_id'] = "currentUser"; 
            userObject['username'] = userDetails.username;
            userObject['password'] = userDetails.password;
            userObject['userid'] = loginDetails.user_id;
            userObject['platform'] = url;
            this.db.put(userObject);
            this.router.navigate(['tabs/tabs/tab1']);
        }
    });
  }
  getUserDetailsFromPouchDB(){
    return this.db.get("currentUser");
  }

  storeHealthDetails(healthData){
    this.db.get("currentUser")
    .then((doc) => {
        doc.healthData = healthData;
        this.db.put(doc);
    }).catch((err) => {
      this.router.navigateByUrl('/login');
    })
  }

}
