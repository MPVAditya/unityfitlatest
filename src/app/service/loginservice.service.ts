import { Injectable } from '@angular/core';
import { HttpserviceService } from './httpservice.service';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {
  constructor(
    private httpService: HttpserviceService,
    private router: Router,
    private authService: AuthService,
  ) {}

  public verifyUser(userDetails: LoginUserDetails) {
    const url =
      userDetails.site !== '' ? userDetails.site : 'https://csi.unityfit.co';
    const apiUrl = url + '/wp-admin/admin-ajax.php';
    const dataToSend: LoginDetailsToSend = { username: userDetails.username, password: userDetails.password, action: userDetails.action };
    this.httpService.postDataToserver(apiUrl, dataToSend).subscribe(data => {
      this.authService.AddLoginDetailsToPouchDB(data, userDetails, url);
    });
  }
}
