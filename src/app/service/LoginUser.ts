class LoginUserDetails {
    username: string;
    password: string;
    site: string;
    action: string;
}

class LoginDetailsToSend {
    username: string;
    password: string;
    action: string;
}