import { Injectable } from '@angular/core';
import { HttpserviceService } from './httpservice.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ForgetpasswordService {

  constructor( private httpService: HttpserviceService, private alertController: AlertController, private router: Router) { }

  forgetPassword(forgetPasswordData:any)
  {
    const url =
    forgetPasswordData.site = 'https://telus.unityfit.co';
    const apiUrl = url + '/wp-json/gratitudechallenge/v1/reset';
    const dataToSend: any = { user_login: forgetPasswordData.username };
    this.httpService.postDataToserver(apiUrl, dataToSend).subscribe(data => {
      this.showPopup();
  });
  }
  
  async showPopup(){
    const alert = await this.alertController.create({
      header: 'Restablecimiento finalizado',
      message: 'El vínculo para restablecer tu contraseña se te ha enviado por correo electrónico. Por favor revisa tu correo electrónico.',
      buttons: [ {
        text: 'Ok',
        role: 'Ok',
        handler: () => {
          this.router.navigateByUrl('/login');
        }
      }]
    });

    await alert.present();
  }
}
