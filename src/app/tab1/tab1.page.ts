import { Component, } from '@angular/core';
import { KnobOptions } from "@xmlking/ngx-knob";
import { faShoePrints } from '@fortawesome/free-solid-svg-icons';
import { faFire } from '@fortawesome/free-solid-svg-icons';
import { faWeight } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { HttpserviceService } from '../service/httpservice.service';
import * as moment from 'moment';
import { Platform } from '@ionic/angular';
import { promise } from 'protractor';
import { Health } from '@ionic-native/health/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  historyOfMeasurements = null;
  measurementDates: any;
  measurementInfo: any;
  fat_percentageYearly: any;
  bmiYearly: any;
  weightYearly: any;
  bmi: any;
  bmiMonthly: any;
  fat_percentage: any;
  fat_percentageMonthly: any;
  weight: any;
  weightMonthly: any;
  torax: any;
  contorno: any;
  estomago: any;
  brazo: any;
  cintura: any;
  cadera: any;
  comments: any;
  nutritionImageFile: any;
  altura: any;
  isSynchonizing: boolean = false;
  dailyData: any;
  lastSync: any;
  todaySteps: any;
  todayDistance: any;
  todayCalories: any;
  goalTextMessage: string;
  value: number = 0;
  constructor(private authService: AuthService, private router: Router, private httpService: HttpserviceService, private platform: Platform) { }

  ngOnInit() {
    this.setupReportView();
    if (!this.isSynchonizing) {
      this.setupHealthDataSync();
    }
  }
  setupReportView() {
    this.historyOfMeasurements = null;
    this.measurementDates = [];
    this.measurementInfo = {};
    this.setupNutritionMeasurementData();
  }

  setupHealthDataSync() {
    this.authService.getUserDetailsFromPouchDB().then(
      (userData) => {
        if (userData && userData.healthData && userData.healthData.length) {
          this.dailyData = userData.healthData.slice(userData.healthData.length - 60);
          this.postDataUpdates();
        }
        this.updateLocalDataWithInteraval();
      });
  }

  setupNutritionMeasurementData() {
    this.authService.getUserDetailsFromPouchDB().then(
      (doc) => {
        let ajaxurl = doc.platform + "/wp-admin/admin-ajax.php";
        let dataToSend = {
          action: 'get_progress_data', inputDate: moment().format('YYYY-MM-DD'), user_id: doc.userid
        }
        this.httpService.postDataToserver(ajaxurl, dataToSend).subscribe(
          dataObject => {
            if (dataObject["yearly"] && dataObject["yearly"][0]) {
              this.fat_percentageYearly = dataObject["yearly"][0].fat_percentageYearly;
              this.bmiYearly = dataObject["yearly"][0].bmiYearly;
              this.weightYearly = dataObject["yearly"][0].weightYearly;
            }
            this.historyOfMeasurements = dataObject.history;
            this.setupMeasurementInfo();
          },
          error => { })
      })
      .catch(err => {
        if (err.status === 404) {
          this.router.navigateByUrl('/login');
        }
      })
  }

  setupMeasurementInfo() {
    let selectedDate = null;
    for (let i = 0; i < this.historyOfMeasurements.length; i++) {
      if (selectedDate && moment(this.historyOfMeasurements[i]["inputDate"]).isAfter(selectedDate)) {
        selectedDate = moment(this.historyOfMeasurements[i]["inputDate"]);
      } else {
        selectedDate = moment(this.historyOfMeasurements[i]["inputDate"]);
      }
      this.measurementDates.push(this.historyOfMeasurements[i]["inputDate"]);
    }
    if (selectedDate) {
      this.measurementInfo.selectedDate = selectedDate.format("YYYY-MM-DD");
      this.updateViewWithDate(selectedDate.format("YYYY-MM-DD"));
    }
  }

  updateViewWithDate(selectedDate) {
    if (this.historyOfMeasurements) {
      for (let i = 0; i < this.historyOfMeasurements.length; i++) {
        if (selectedDate == this.historyOfMeasurements[i]["inputDate"]) {
          this.bmi = this.historyOfMeasurements[i].bmi;
          this.bmiMonthly = this.historyOfMeasurements[i].bmiMonthly;
          this.fat_percentage = this.historyOfMeasurements[i].fat_percentage;
          this.fat_percentageMonthly = this.historyOfMeasurements[i].fat_percentageMonthly;
          this.weight = this.historyOfMeasurements[i].weight;
          this.weightMonthly = this.historyOfMeasurements[i].weightMonthly;

          this.torax = this.historyOfMeasurements[i].torax;
          this.contorno = this.historyOfMeasurements[i].contorno;
          this.estomago = this.historyOfMeasurements[i].estomago;
          this.brazo = this.historyOfMeasurements[i].brazo;
          this.cintura = this.historyOfMeasurements[i].cintura;
          this.cadera = this.historyOfMeasurements[i].cadera;

          this.comments = this.historyOfMeasurements[i].comments;
          this.nutritionImageFile = this.historyOfMeasurements[i].nutrition_file;
          this.altura = this.historyOfMeasurements[i].altura;
        }
      }
    }
  }

  postDataUpdates() {
    this.updateDashData();
  }
  updateDashData() {
    this.setupProgressChart();
    var todayData = this.getDataForDay_GoogleBugFix(moment(), this.dailyData);
    this.todaySteps = this.numberWithCommas(todayData.steps);
    this.todayDistance = this.numberWithCommas(todayData.distance);
    this.todayCalories = this.numberWithCommas(todayData.calories);
  }
  setupProgressChart() {
    let Totalsteps = this.calculateCompletionPercentageOfSteps();
    this.value = this.numberWithCommas(Totalsteps);
  }
  calculateCompletionPercentageOfSteps() {
    var todayStepCount = 0;
    if (this.dailyData) {
      for (let i = 0; i < this.dailyData.length; i++) {
        if (moment(this.dailyData[i]["startDate"]).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) {
          todayStepCount = this.dailyData[i]["steps"];
        }
      }
    }

    if (todayStepCount < 10000) {
      this.goalTextMessage = "Alcanza 10,000 pasos al día";
      return todayStepCount / 10000 * 100;
    } else if (todayStepCount < 20000) {
      this.goalTextMessage = "Próxima meta - 20,000 pasos";
      return todayStepCount / 20000 * 100;
    } else if (todayStepCount < 30000) {
      this.goalTextMessage = "Próxima meta - 30,000 pasos";
      return todayStepCount / 30000 * 100;
    } else if (todayStepCount < 50000) {
      this.goalTextMessage = "Próxima meta - 50,000 pasos";
      return todayStepCount / 50000 * 100;
    } else {
      this.goalTextMessage = "¡Todas las metas alcanzadas!";
      return 100;
    }
  }
  getDataForDay_GoogleBugFix(day, healthData) {
    if (healthData) {
      for (let i = 0; i < healthData.length; i++) {
        let stepDataDate = moment(new Date(healthData[i].startDate));
        if (stepDataDate.format("YYYY-MM-DD") == day.format("YYYY-MM-DD")) {
          if (healthData[i].steps == 0 && i > 0) {
            return healthData[i - 1];
          } else {
            return healthData[i];
          }
        }
      }
      return { steps: 0, distance: 0, calories: 0 }
    } else {
      return { steps: 0, distance: 0, calories: 0 }
    }
  }

  updateLocalDataWithInteraval() {
    if (!this.lastSync) {
      this.updateLocalData();
    } else {
      var duration = moment.duration(moment().diff(this.lastSync));
      var minutes = duration.asMinutes();
      if (minutes > 2) {
        this.updateLocalData();
      }
    }
  }


  updateLocalData() {
    this.isSynchonizing = true;
    this.lastSync = moment();
    this.authService.getUserDetailsFromPouchDB()
      .then((userData) => {
        console.log(userData);
        this.getFitnessDataForUser()
          .then((data) => {
            console.log(data);
            if (data) {
              let addToArray = [data];
              this.authService.storeHealthDetails(addToArray);
              this.dailyData = addToArray.slice(addToArray.length - 60);
            }
            this.isSynchonizing = false;
            this.postDataUpdates();
          });
      });
  }
  numberWithCommas(x) {
    if (x) {
      let parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    } else {
      return ""
    }
  }


  knOptions = {
    readOnly: true,
    size: 180,
    unit: '',
    textColor: '#00C2BC',
    fontSize: '14',
    max: 100,
    trackWidth: 19,
    barWidth: 20,
    trackColor: '#D8D8D8',
    barColor: '#00C2BC',
    offset: 7,
    subText: {
      enabled: true,
      font: '11',
      text: 'Pasos de hoy',
      color: '#00C2BC',
      offset: 0,
    },
  }

  faShoePrints = faShoePrints;
  faFire = faFire;
  faWeight = faWeight;


  //to be added in a service 

  getFitnessDataForUser() {
    let dataPromise = new Promise((resolve, reject) => {
      let numOfHistoryToUpdate = 180;
      let datatypes = [{
        read: ['distance', 'steps', 'calories'] // Read only permission
      }]
      if (navigator && navigator['health']) {
        if (this.platform.is('android')) {
          navigator['health'].promptInstallFit(postFitInstall, errorCallback)
        } else {
          postFitInstall();
        }

      } else {
      }

      function postFitInstall() {
        navigator['health'].requestAuthorization(datatypes, successCallback1, errorCallback);
      }


      function successCallback1(message) {
        var todayStartDate = new Date();
        todayStartDate.setHours(0, 0, 0, 0);

        navigator['health'].queryAggregated({
          startDate: new Date(new Date().getTime() - numOfHistoryToUpdate * 24 * 60 * 60 * 1000),
          endDate: new Date(), // now
          dataType: 'steps',
          bucket: 'day',
          filtered: true
        }, successCallbackSteps, errorCallback)

        navigator['health'].queryAggregated({
          startDate: new Date(new Date().getTime() - numOfHistoryToUpdate * 24 * 60 * 60 * 1000),
          endDate: new Date(), // now
          dataType: 'calories',
          bucket: 'day',
          filtered: true
        }, successCallbackCalories, errorCallback)

        navigator['health'].queryAggregated({
          startDate: new Date(new Date().getTime() - numOfHistoryToUpdate * 24 * 60 * 60 * 1000),
          endDate: new Date(), // now
          dataType: 'distance',
          bucket: 'day',
          filtered: true
        }, successCallbackDistance, errorCallback)
      }


      function successCallbackSteps(data) {
        successCallbackQueryAggregated(data, "steps");
      }

      function successCallbackCalories(data) {
        successCallbackQueryAggregated(data, "calories");
      }

      function successCallbackDistance(data) {
        successCallbackQueryAggregated(data, "distance");
      }

      var dataItem = {};
      var dataItemIndex = 0;

      function successCallbackQueryAggregated(data, storeID) {
        dataItem[storeID] = data;
        dataItemIndex++;
        if (dataItemIndex == 3) {
          resolve(concatenateDataItems(dataItem))
        }
      }

      function concatenateDataItems(localDataItems) {
        var combinedDataItems = [];
        var lengthOfItems = localDataItems.steps.length;
        for (var i = 0; i < lengthOfItems; i++) {
          var dataObject = {};
          dataObject['steps'] = localDataItems.steps[i].value
          dataObject['distance'] = localDataItems.distance[i].value
          dataObject['calories'] = localDataItems.calories[i].value
          dataObject['startDate'] = localDataItems.steps[i].startDate
          dataObject['endDate'] = localDataItems.steps[i].endDate
          combinedDataItems.push(dataObject)
        }
        return combinedDataItems;
      }


      function errorCallback(message) {
        resolve(null)
      }
    });

    return dataPromise;
  }

}