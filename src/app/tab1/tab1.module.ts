import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { KnobModule } from "@xmlking/ngx-knob";
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab1Page,  data: {animation: 'Tab1Page'} }]),
    KnobModule,
    FontAwesomeModule
  ],
  declarations: [Tab1Page]
})
export class Tab1PageModule {}
