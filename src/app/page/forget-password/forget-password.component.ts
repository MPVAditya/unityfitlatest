import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ForgetpasswordService } from 'src/app/service/forgetpassword.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss'],
})
export class ForgetPasswordComponent implements OnInit {

  constructor(private fb: FormBuilder, private forgetPasswordService: ForgetpasswordService) { }

  ngOnInit() {}

  forgetPasswordForm = this.fb.group({
    username: ['', Validators.required],
    site: ['']
  });

  onSubmit()
  {
    this.forgetPasswordService.forgetPassword(this.forgetPasswordForm.value);
    this.forgetPasswordForm.reset();
  }

}
