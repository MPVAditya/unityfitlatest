import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { PageRoutingModule } from './page-routing.module';



@NgModule({
  declarations: [LoginComponent,ForgetPasswordComponent],
  imports: [
    IonicModule,
    CommonModule,
    PageRoutingModule,
    ReactiveFormsModule,
  ],
  exports: [ReactiveFormsModule]
})
export class PageModule { }
